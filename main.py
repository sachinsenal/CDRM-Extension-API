from decrypt import decrypt_content
import json
from flask import Flask, request
from database_check import database_check

app = Flask(__name__)

database_check()


@app.route('/', methods=['POST'])
def post_endpoint():

    # Get the JSON data from the request
    data = json.loads(request.data.decode())
    print(json.dumps(data, indent=4))

    # Get the PSSH
    pssh = data['pssh']

    # Get the license URL
    lic_url = data['license_url']

    # Get the headers
    headers = data['headers']

    # Get the MPD url
    mpd = data['manifest_url']

    # Format the headers

    # Split the string into lines
    lines = headers.strip().split('\n')

    # Create an empty dictionary to store the key-value pairs
    headers_dict = {}

    # Iterate through each line and split it into key-value pairs
    for line in lines:
        key, value = line.split(': ', 1)  # Split only at the first occurrence of ': ' to handle values containing ': '
        headers_dict[key] = value

    print(json.dumps(headers_dict, indent=4))

    try:
        keys = decrypt_content(in_pssh=pssh, license_url=lic_url, headers=headers_dict, mpd=mpd)
        return {"keys": keys}
    except Exception as error:
        return {"keys": [f'{error}']}


if __name__ == '__main__':
    app.run(debug=True)
